SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
ROOT_DIR="$( cd -P "$( dirname "$SOURCE" )" && cd -P ".." && pwd )"
date_init=`date "+%Y_%m_%d"`
time_init=`date "+%Y%m%d_%H_%M_%S"`

second_init=`date +%s`
tool_name="python_hive_tool_cloudera"
usage () {
  echo "
  ${0} {phase} {engine} {database} {table} {column_result file}

  REQUIRED PARAMETERS:
   + phase:             pass the phase you would like to execute.  phases listed below should be executed in specified order.
                        - profile_table
                        - profile_column
                        - profile_column_details
   + engine:            hive or spark

  "
}

source $ROOT_DIR"/$tool_name/lib/shell_lib.sh"
readConfig "$ROOT_DIR/$tool_name"
requireParam phase $1
requireParam engine $2
database=$3
table=$4
inputFile=$5
echo $date_init
createDirectory $ROOT_DIR"/log/$date_init"
logDir=$ROOT_DIR"/log/$date_init"

profile_table() {

record_start_time "table"
python $ROOT_DIR"/$tool_name/lib/profileTableWrapper.py" $database $table $engine
requireProcessSuccess "profile Table"
record_end_time "table"
time_dif time_table "table"

}

profile_column () {

time_flag=`date "+%Y%m%d_%H_%M_%S"`
record_start_time "column"
requireFile $inputFile
if [[ "${engine}" == "hive" ]]
  then
    spark-submit $ROOT_DIR"/$tool_name/lib/profileColumnWrapper.py" $inputFile $database $table $engine false 2>&1 | tee "$logDir/profile_column_$database_$table_$time_flag"
else
/usr/bin/spark-submit --master yarn-client  --num-executors 40 --executor-memory 4G --executor-cores 2  --driver-memory 8G $ROOT_DIR"/$tool_name/lib/profileColumnWrapper.py" $inputFile $database $table $engine false 2>&1 | tee "$logDir/profile_column_$database_$table_$time_flag"
fi
requireProcessSuccess "profile column"
record_end_time "column"
time_dif time_table "column"

}

profile_column_details () {
time_flag=`date "+%Y%m%d_%H_%M_%S"`
record_start_time "column_details"
requireFile $inputFile
/usr/bin/spark-submit --master yarn-client  --num-executors 40 --executor-memory 4G --executor-cores 2  --driver-memory 8G $ROOT_DIR"/$tool_name/lib/profileColumnWrapper.py" $inputFile $database $table $engine true 2>&1 | tee "$logDir/profile_column_details_$database_$table_$time_flag"
requireProcessSuccess "profile column details"
record_end_time "column_details"
time_dif time_table "column_details"

}


case $phase in
profile_table)
  profile_table
  ;;
profile_column)
  profile_column
  ;;
profile_column_details)
  profile_column_details
  ;;

*)
  echo "Invalid phase supplied"
  usage
  exit -1
  ;;
esac
