"""
    File name: ProfileTableWrapper.py
    Author: Guohao Xiao
    Date created: 12/06/2016
    Date last modified: 12/06/2016
    Python Version: 2.7
    improvement: wrapper of profiling tables
"""
import time
import sys
import os
import datetime
from profileTable import profileTable
import ConfigParser 
from OsUtility import * 

if __name__ == "__main__":
    table='all'
    database='gxiao'


    settings = ConfigParser.ConfigParser() 
    settings.read('/home/guohaoxiao/python_hive_tool_cloudera/config.ini') 
    data_dir=settings.get('configure', 'data_dir')
    log_dir=settings.get('configure', 'log_dir') 

    if sys.argv[2:]:
        table=(str)(sys.argv[2])
        database=(str)(sys.argv[1])

    if sys.argv[1:]:
        database=(str)(sys.argv[1])
    
    today=getToday()
    data_dir=data_dir+"/"+today
    log_dir=log_dir+"/"+today
    make_sure_path_exists(data_dir)
    make_sure_path_exists(log_dir)

    handle=profileTable(database,table,data_dir,log_dir)
    handle.process()
