#import numpy
import time
from pyspark.sql import HiveContext
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
#from pyspark.mllib.linalg import *
from SparkDataFunctions import *
import sys
import re
from pyspark.mllib.feature import Word2Vec

class SparkProfiling:

    def __init__(self,appName,logDir,rootDir,libDir,spark_log_level):
        self.conf = SparkConf().setAppName(appName).set("spark.local.dir", rootDir+"/local").set("SPARK_TMPDIR",rootDir+"/tmp").set("java.io.tmpdir",rootDir+"/javatmp");
        self.sc = SparkContext(conf=self.conf,pyFiles=[libDir+'/lib/SparkProfiling.py',libDir+'/lib/SparkDataFunctions.py'])
        self.sc.setLogLevel(spark_log_level)
        self.sqlContext = HiveContext(self.sc)
        self.rddLookup={}

    def rddCache(self,database,table,column_list):
        if column_list is None:
            key="|".join((database,table,"*"))
            if key in self.rddLookup:
                return self.rddLookup[key]
            else:
                rdd=self.sqlContext.sql("select *  from "+database+"."+table).cache()
            self.rddLookup[key]=rdd
            return self.rddLookup[key]
      
        key="|".join((database,table,",".join(column_list)))
        if key in self.rddLookup:
            return self.rddLookup[key]
        else: 
            rdd=self.sqlContext.sql("select "+",".join(column_list)+"  from "+database+"."+table).cache()
            self.rddLookup[key]=rdd
            return self.rddLookup[key]

    def getStatistics(self,columnList,database,tableName):
        """
        return the min max mean, stdev, count for each column 
        """
        cStats = self.rddCache(database,tableName,columnList)
        statistics=None
        if columnList is None:
            statistics= cStats.describe().map(lambda x:flattenStatistic(x)).reduce(mergeDict)
        else:
            statistics= cStats.describe(columnList).map(lambda x:flattenStatistic(x)).reduce(mergeDict)
        return statistics 
#                                 0       1            2            3            4       5            6         7                 8                9    10  11   12
#Database,Table Name,Column Name,count,distinct_count,null_count,empty_count,MF_value,MF_value_count,avg,MF_value_not_null,MF_value_count_not_null,min,max,mean,stdev 
#gxiao,snf,provider_id,76901,13524,0,0,215145,26,291362.582377,215145,26,100048,95039,291356.5716440618,169733.46930741024 
    def getCustomStatistics(self,database,tableName,columnList): 

        statistics=self.getStatistics(columnList,database,tableName)
        flatten_rdd = self.rddCache(database,tableName,columnList).flatMap(flatten)
        column_value_count = flatten_rdd.map(lambda x:(x,1)).reduceByKey(lambda x,y:x+y).map(lambda x:(x[0][0],(x[0][1],x[1],1)))
        results=column_value_count.aggregateByKey((0,0,0,0,'0',0,0,'0',0),mergeRecord,mergeResult).map(lambda x:joinStatistics(x,statistics)).map(lambda x:(x[0],x[1],x[2],x[3],percent(float(x[3])/float(x[1])),x[4],percent(float(x[4])/float(x[1])),x[5],x[6],percent(float(x[6])/float(x[1])),x[8],x[9],percent(float(x[9])/float(x[1])),x[10],x[11],x[12],x[13])).collect()
        return results

    def getCustomDistribution(self,database,tableName,columnList):
        
        flatten_rdd = self.rddCache(database,tableName,columnList).flatMap(flatten)
        column_value_count = flatten_rdd.map(lambda x:(x,1)).reduceByKey(lambda x,y:x+y).map(lambda x:((x[0][0],x[1],x[0][1]),1)).sortByKey(False).groupBy(lambda x:x[0][0]).map(lambda x:(x[0],[(i[0][2],i[0][1]) for i in x[1]])).collect()
        return column_value_count

    def getCustomPatternDistribution(self,database,tableName,columnList):

        flatten_rdd = self.rddCache(database,tableName,columnList).flatMap(flatten)
        column_value_count = flatten_rdd.map(lambda x:((x[0],convertToPattern(x[1])),1)).reduceByKey(lambda x,y:x+y).map(lambda x:((x[0][0],x[1],x[0][1]),1)).sortByKey(False).groupBy(lambda x:x[0][0]).map(lambda x:(x[0],[(i[0][2],i[0][1]) for i in x[1]])).collect()
        return column_value_count

    def getPatternMatch(self,database,tableName,column,pattern):
        flatten_rdd = self.sqlContext.sql("select "+column+"  from "+database+"."+tableName).flatMap(flatten).cache()
        match_rdd = flatten_rdd.map(lambda x:("matched" if re.match( pattern, x[1], re.M|re.I) else "not_matched" ,1)).countByKey()
        return match_rdd
    def getRoleMatch(self,database,tableName,column,role):
        flatten_rdd = self.sqlContext.sql("select "+column+"  from "+database+"."+tableName+" where "+role).flatMap(flatten).count()
        return flatten_rdd

    def getSynonyms(self,database,tableName,value,count):
        flatten_rdd = self.rddCache(database,tableName,None).map(flattenToValues)
        word2vec = Word2Vec()
        model = word2vec.fit(flatten_rdd)
        synonyms = model.findSynonyms(value, count)
        return synonyms

    def getSynonymsModel(self,database,tableName,fraction):
        flatten_rdd = self.rddCache(database,tableName,None).map(flattenToValues).sample(False,float(fraction),int(round(time.time() * 1000))).cache()
        word2vec = Word2Vec()
        model = word2vec.fit(flatten_rdd)
        return model


    def getTopNValue(self,database,tableName,columnList,count):
        count=int(count)
        flatten_rdd = self.rddCache(database,tableName,columnList).flatMap(flatten).cache()
        column_value_count = flatten_rdd.map(lambda x:(x,1)).reduceByKey(lambda x,y:x+y).map(lambda x:(x[0][0],(x[0][1],x[1])))
        topN = column_value_count.combineByKey(lambda x:(count,[(x[1],x[0])]),mergeRecordTopN,mergeResultTopN).map(lambda x:(x[0],x[1][0],sorted([swap(heappop(x[1][1])) for i in range(len(x[1][1]))],key=lambda x: x[1],reverse=True))).collect()
        return  topN

    def getCount(self,database,tableName,columnList):
        return self.rddCache(database,tableName,columnList).count()

    def getTopNValueSynonymns(self,database,tableName,columnList,count,synonymnsCount,inputTopN,fraction):
        topN=inputTopN
        if inputTopN is None:
            topN=self.getTopNValue(database,tableName,columnList,count)
        model=self.getSynonymsModel(database,tableName,fraction)
        result=[]
       # print str(topN)
       # for i in model.getVectors():
       #     print str(i)
        for column in topN:
            columnName=column[0]
            count=column[1]
            list=column[2]
            subResult=[]
            for element in list:
                try:
                    synonyms = model.findSynonyms(element[0].decode("utf-8"), int(synonymnsCount))
                    subResult.append((element[0],synonyms))
                except Exception as e:
                    # java.lang.IllegalStateException: Licensed Practical Nurse not in vocabulary
                    print str(e)
                    pass 
            result.append((columnName,subResult))
        return result
          
        


#test=SparkProfiling("test","~/log","../","../")
#print test.getPatternMatch("gxiao","sampleData",'id',r'2')
#print test.getRoleMatch("gxiao","sampleData","id","id>2")
#print test.getCustomDistribution("gxiao","flight",['dest','weatherdelay'])
#print test.getCustomPatternDistribution("gxiao","flight",['dest','weatherdelay'])
#print test.getSynonyms("gxiao","flight","1",10)
#topN=test.getTopNValue("gxiao","flight",['dest','weatherdelay'],2)
#print list[0][2][1][0] is None
#topN=None
#print test.getTopNValueSynonymns("gxiao","flight",['dest','weatherdelay'],2,2,topN,0.1)

