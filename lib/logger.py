"""
logger class
"""
import logging
 
class logger:
    """
    loggger encapsulates the format, cconsole stream and a file stream
    """
    def __init__(self, LogFileName):
        """
        construction function
        """
        logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s - %(name)s - %(threadName)s - %(levelname)s - %(message)s",
                    datefmt='%m-%d %H:%M',
                    filename=LogFileName,
                    filemode='w')
        console = logging.StreamHandler()
        console.setLevel(logging.INFO)
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(threadName)s - %(levelname)s - %(message)s")
        console.setFormatter(formatter)

        self.logging=logging
        self.console=console

    def getLogger(self,name=__name__):
        """
        get the logger instance and configure its module name
        """     
        self.logging.getLogger(name).addHandler(self.console)
        return self.logging.getLogger(name)

    def setConsoleLevel(self,name=__name__,level=logging.INFO):
        self.logging.getLogger(name).removeHandler(self.console)
        console = self.logging.StreamHandler()
        console.setLevel(level)
        formatter = self.logging.Formatter("%(asctime)s - %(name)s - %(threadName)s - %(levelname)s - %(message)s")
        console.setFormatter(formatter)
        logging.getLogger(name).addHandler(console)

    def setLevel(self,name=__name__,level=logging.INFO):
        self.logging.getLogger(name).setLevel(level)

