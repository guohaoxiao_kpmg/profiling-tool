"""
    File name: SqoopUtil.py
    Author: Guohao Xiao
    Date created: 12/06/2016
    Date last modified: 12/06/2016
    Python Version: 2.7
"""

import os


class SqoopUtil:
    def __init__(self):
        print 'init'

    def configure(self,libjars,driver,connect,username,password,mapper):
        self.libjars=libjars
        self.driver=driver
        self.connect=connect
        self.username=username
        self.password=password
        self.mapper=mapper

    def buildStatement_int(self,prefix,value):
         return prefix+" " +str(value)+" \\\n"

    def buildStatement(self,prefix,value):
        return prefix+" \'" +value+"\' \\\n"
    def buildStatement_double(self,prefix,value):
         return prefix+" \"" +value+"\" \\\n"

    def query(self,query):
        queryStatement="sqoop  eval \\\n"
        queryStatement=self.buildStatement(queryStatement+"-libjars",self.libjars)
        queryStatement=self.buildStatement(queryStatement+"--driver",self.driver)
        queryStatement=self.buildStatement(queryStatement+"--connect",self.connect)
        queryStatement=self.buildStatement(queryStatement+"--username",self.username)
        queryStatement=self.buildStatement(queryStatement+"--password",self.password)
        queryStatement=self.buildStatement(queryStatement+"--query",query)        
        print queryStatement[0:-2]
        result=os.popen(queryStatement).read()
        print "result:"
        print result

    def export(self,table,export_dir,fields_teminator,lines_teminator,null_string,null_non_string,mapper):
        queryStatement=""
        queryStatement=self.buildStatement(queryStatement+"sqoop  export -libjars",self.libjars)
        queryStatement=self.buildStatement_int(queryStatement+"-m",mapper)
        queryStatement=self.buildStatement(queryStatement+"--driver",self.driver)
        queryStatement=self.buildStatement(queryStatement+"--connect",self.connect)
        queryStatement=self.buildStatement(queryStatement+"--username",self.username)
        queryStatement=self.buildStatement(queryStatement+"--password",self.password)
        queryStatement=self.buildStatement(queryStatement+"--table",table)
        queryStatement=self.buildStatement(queryStatement+"--export-dir",export_dir)
        queryStatement=self.buildStatement_double(queryStatement+"--fields-terminated-by",fields_teminator)
        queryStatement=self.buildStatement_double(queryStatement+"--lines-terminated-by",lines_teminator)
        queryStatement=self.buildStatement(queryStatement+"--input-null-string",null_string)
        queryStatement=self.buildStatement(queryStatement+"--input-null-non-string",null_non_string)
        queryStatement=queryStatement+"--batch"
        print queryStatement
        result=os.popen(queryStatement).read()
        print "result:"
        print result

    def importToHive(self,target_dir,split_by,mapper,query,null_string,null_non_string,fields_teminator):
        queryStatement="hadoop dfs -rmr "+target_dir+"\n"
        queryStatement=self.buildStatement(queryStatement+"sqoop  import -libjars",self.libjars)
        queryStatement=self.buildStatement_int(queryStatement+"-m",mapper)
        queryStatement=self.buildStatement(queryStatement+"--driver",self.driver)
        queryStatement=self.buildStatement(queryStatement+"--connect",self.connect)
        queryStatement=self.buildStatement(queryStatement+"--username",self.username)
        queryStatement=self.buildStatement(queryStatement+"--password",self.password)
        queryStatement=queryStatement+"--as-textfile \\\n"
        queryStatement=self.buildStatement(queryStatement+"--query",query)
        queryStatement=self.buildStatement(queryStatement+"--target-dir",target_dir)
        queryStatement=self.buildStatement(queryStatement+"--split-by",split_by)
        queryStatement=self.buildStatement(queryStatement+"--fields-terminated-by",fields_teminator)
        queryStatement=self.buildStatement(queryStatement+"--null-string",null_string)
        queryStatement=self.buildStatement(queryStatement+"--null-non-string",null_non_string)
        queryStatement=queryStatement+"--hive-drop-import-delims"
        print queryStatement
        result=os.popen(queryStatement).read()
        print "result:"
        print result

    def getTableDescription(self,db,table):
        queryStatement=""
        queryStatement=self.buildStatement(queryStatement+"sqoop eval -libjars",self.libjars)
        queryStatement=self.buildStatement(queryStatement+"--driver",self.driver)
        queryStatement=self.buildStatement(queryStatement+"--connect",self.connect)
        queryStatement=self.buildStatement(queryStatement+"--username",self.username)
        queryStatement=self.buildStatement(queryStatement+"--password",self.password)
        queryStatement=self.buildStatement_double(queryStatement+"--query","use "+db+";select * from information_schema.columns where table_name=\'"+table+"\' order by ordinal_position")
        queryStatement=queryStatement[0:-2]
        print  queryStatement
        result=os.popen(queryStatement).read()
        print "result:"
        set=[]
        for line in result.split("\n"):
            
            if "|" in line:
              set.append(line.split("|")[4].strip()+"\t"+line.split("|")[8].strip())  
        return set
        
        
    
    def createTable(self,table,colDic):
        cmd="create table "+table+"(\n"
        for key in colDic:
            cmd=cmd+key+" "+colDic[key]+","
        cmd=cmd[0:-1]+")"
        self.query(cmd)
    def interactiveMode(self):
        cmd=""
        while cmd!="quit" and cmd!="q":
            cmd = str(raw_input("input your query, type q to quit:\n"))
            if cmd!="q":
                self.query(cmd)   
       


   
