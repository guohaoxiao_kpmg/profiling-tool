import os
import errno
import datetime

def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST or not os.path.isdir(path):
            raise

def getToday():
    return datetime.datetime.now().strftime("%Y_%m_%d")
