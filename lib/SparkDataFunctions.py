#import numpy
from pyspark.sql import HiveContext
from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
#from pyspark.mllib.linalg import *
from heapq import *
def percent(num):
    return '{0:.4%}'.format(num) 

def isNumber(num):
    
    return str(num).lstrip('-').replace('.','',1).isdigit()

def flatten(x):
    list=[]
    for key in x.asDict().keys():
        list.append((key,x[key].encode('utf-8') if isinstance(x[key], unicode) else str(x[key]) ))
    return list

def flattenToValues(x):
    list=[]
    for key in x.asDict().keys():
        list.append(x[key].encode('utf-8') if isinstance(x[key], unicode) else str(x[key]))
    return list


def flattenStatistic(x):
    dic={}
    category=x["summary"]
    dic[category]={}

    for key in x.asDict().keys():
        if key=="summary":
            continue
        dic[category][key]=x[key]
    return dic


def mergeDict(x,y):
    x.update(y)
    return x

#Column Name,Record Count,Min Value,Max Value,Count of distinct value,Count of NULL-valued records,% of NULL-valued Record,#1 Value (count by value in desc order, the 1st non-NULL value),Count of #1 valued record,% of #1 valued record
def swap(x):
    return (x[1],x[0])
def mergeRecordTopN(subresult,record):
    list=subresult[1]
    count=subresult[0]
    heappush(list,(record[1],record[0]))
    while  len(list)>count:
        heappop(list)
    return (count,list)

def mergeResultTopN(subA,subB):
    count=subA[0]
    list=subA[1]
    for record in subB[1]:
        heappush(list,record)
    while  len(list)>count:
        heappop(list)
    return (count,list)

def mergeRecord(subresult,record):
    count = int(record[1])+int(subresult[0])
    distinct_count = record[2]+subresult[1]
    null_count = subresult[2]+(0 if record[0] is not None else record[1])
    empty_count = subresult[3]+(0 if record[0] is not None and record[0]!="" else record[1])
    MF_value_count = int(record[1]) if int(record[1])>subresult[5] else subresult[5]
    MF_value =  record[0] if float(record[1])>subresult[5] else subresult[4]
    MF_value_count_not_null = int(record[1]) if record[0] is not None and int(record[1])>subresult[8] and record[0]!="Null" and ((str)(record[0])+"").strip()!="" else subresult[8]
    MF_value_not_null =  record[0] if record[0] is not None and int(record[1])>subresult[8] and record[0]!="Null" and ((str)(record[0])+"").strip()!=""   else subresult[7]
    avg=None
    #if isNumber(record[0]) and isNumber(subresult[6]):
    #    avg=float((float(record[0])*int(record[1])+float(subresult[6])*int(subresult[0]))/count)
    #elif isNumber(record[0]):
    #    avg=float(record[0])
    #elif isNumber(subresult[6]):
    #    avg=float(subresult[6])
    #else:
    #    avg=None
    return (count,distinct_count,null_count,empty_count,MF_value,MF_value_count,avg,MF_value_not_null,MF_value_count_not_null)

def mergeResult(subA,subB):
    count = subA[0]+subB[0]
    distinct = subA[1]+subB[1]
    null_count =  subA[2]+subB[2]
    empty_count =  subA[3]+subB[3]
    MF_value_count = subB[5] if subB[5]>subA[5] else subA[5]
    MF_value =  subB[4] if subB[5]>subA[5] else subA[4]
    MF_value_count_not_null = subB[8] if subB[8]>subA[8] else subA[8]
    MF_value_not_null =  subB[7] if subB[8]>subA[8] else subA[7]
    avg=None
    #if isNumber(subB[6]) and isNumber(subA[6]):
    #    avg=float((float(subB[6])*subA[0]+float(subA[6])*subB[0])/count)
    #elif isNumber(subB[6]):
    #    avg=subB[6]
    #elif isNumber(subA[6]):
    #    avg=subA[6]
    #else:
    #    avg=None
    return (count,distinct,null_count,empty_count,MF_value,MF_value_count,avg,MF_value_not_null,MF_value_count_not_null)


def joinStatistics(x,statistics):
    column=x[0]
    return (x[0],)+x[1]+(statistics.get("min").get(column),statistics.get("max").get(column),statistics.get("mean").get(column),statistics.get("stddev").get(column))


def convertToPattern(x):
    #x=x.encode('utf-8')
    if x.lower()=="none" or x.lower()=="null":
        return x
    result=""
    for c in x:
        if c.isdigit():
            result=result+"d"
        elif c.isalpha():
            result=result+"a"
        else:
            result=result+c
    return result
