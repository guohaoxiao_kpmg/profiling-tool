"""
    File name: ProfileColumn.py
    Author: Guohao Xiao
    Date created: 12/06/2016
    Date last modified: 12/06/2016
    Python Version: 2.7
    improvement: combine statistic query for each column into one query by using union all, but maybe one row is better
                 mutiple threads version
"""


import time
import sys
import datetime
import subprocess
import os
import threading
from HiveUtil import *
from logger import *
from OsUtility import *

class profileColumns:
    """
    profile in the column level
    """
    hiveUtil=""
    def __init__(self,fileName,numThreads,dataDir,logDir,rootDir,libDir,TopN_count,TopNValueSynonymns_count,TopNValueSynonymns_synonymns_count,TopNValueSynonymns_sample_percent,spark_log_level):
        #to do add time stamp here 
        log = logger(logDir+"/"+__name__+".log")
        self.logging=log.getLogger(__name__)
        self.hiveUtil=HiveUtil(log)
        self.logging.info("init profilieColumns")
        self.fileName=fileName
        self.numThreads=numThreads
        self.result=[]
        self.details_result=[]
        self.tasks={}
        self.tasks_details={}
        self.dataDir=dataDir
        self.resultFile=dataDir+"/column_details.csv"
        self.rootDir=rootDir
        self.libDir=libDir
        self.logDir=logDir
        self.spark_log_level=spark_log_level
        #self.spark=SparkProfiling("spark",self.logDir,self.rootDir,self.libDir,self.spark_log_level)
        self.TopN_count=TopN_count
        self.TopNValueSynonymns_count=TopNValueSynonymns_count
        self.TopNValueSynonymns_synonymns_count=TopNValueSynonymns_synonymns_count
        self.TopNValueSynonymns_sample_percent=TopNValueSynonymns_sample_percent


    def setFile(self,fileName):
        self.fileName=fileName

    def setNumThreads(self,numThreads):
        self.numThreads=numThreads

    def readFile(self):
        self.logging.info("reading file: "+self.fileName)
        f = open(self.fileName,'r')
        lines = f.readlines()
        for line in lines:
            array = line.split(',')
            if array[-2].replace('\n','')=='1':
                database=array[0]
                tableName=array[1]
                column=array[2]
                if self.tasks.get(database) is None:
                    self.tasks[database]={}
                if  self.tasks.get(database).get(tableName) is None:
                    self.tasks[database][tableName]=[column]
                else:
                    self.tasks[database][tableName].append(column)

            if array[-1].replace('\n','')=='1':
                database=array[0]
                tableName=array[1]
                column=array[2]
                if self.tasks_details.get(database) is None:
                    self.tasks_details[database]={}
                if  self.tasks_details.get(database).get(tableName) is None:
                    self.tasks_details[database][tableName]=[column]
                else:
                    self.tasks_details[database][tableName].append(column)
     
    
    def columnProfile(self,database,table,columns):
        self.logging.info("profiling for columns: "+str(columns)+" at "+database+"."+table)
        query=[]
        for column in columns:
            value=self.runFristNonNull(column,database,table)
            query.append(self.getStaticsQuery(column,str(value),database,table))
        subResult=[",".join(x) for x in self.hiveUtil.query("\nunion all\n".join(query))]
        lock.acquire()
        self.result.extend(subResult)
        lock.release()

    def sparkProcess(self):
        from SparkProfiling import SparkProfiling
        self.spark=SparkProfiling("spark",self.logDir,self.rootDir,self.libDir,self.spark_log_level)
        self.readFile()
        for database in self.tasks:
            for table in self.tasks[database]:
                results=self.spark.getCustomStatistics(database,table,self.tasks[database][table])
                for result in results:
                    result= [str(i) for i in result]
                    self.result.append(database+","+table+","+",".join(result))
        self.storeToFileSpark()
    
    def sparkProcess_details(self): 
        from SparkProfiling import SparkProfiling
        self.spark=SparkProfiling("spark",self.logDir,self.rootDir,self.libDir,self.spark_log_level)      
        self.readFile()
        for database in self.tasks_details:
            for table in self.tasks_details[database]:
                columnDesc=self.hiveUtil.describeTable(database,table)
                count=self.spark.getCount(database,table,self.tasks_details[database][table])
                customPatternDistribution=self.spark.getCustomPatternDistribution(database,table,self.tasks_details[database][table])
                topN=self.spark.getTopNValue(database,table,self.tasks_details[database][table],self.TopN_count)
                topNSynoymns=self.spark.getTopNValueSynonymns(database,table,self.tasks_details[database][table],self.TopNValueSynonymns_count,self.TopNValueSynonymns_synonymns_count,topN,self.TopNValueSynonymns_sample_percent)
                self.storeToFileSparkDetails(database,table,self.tasks_details[database][table],customPatternDistribution,topN,topNSynoymns,count,columnDesc)



    def process(self):
        threads=[]
        global lock
        lock=threading.Lock()
        self.readFile()
        for database in self.tasks:
            for table in self.tasks[database]:
                while threading.activeCount()>self.numThreads:
                    time.sleep(3)
                t=threading.Thread(name=database+"."+table+" Thread", target=self.columnProfile,args=(database,table,self.tasks[database][table]))
                t.start()
                threads.append(t)
        for t in threads:
            t.join()     
        self.storeToFile()

    # plan to use the below method, but not yet supported
    #select year,rank() over (partition by year order by count(*) desc) as year_rank, month,rank() over (partition by month order by count(*) desc) as month_rank   from flight;
    #FAILED: SemanticException Failed to breakup Windowing invocations into Groups. At least 1 group must only depend on input columns. Also check for circular dependencies.
    #Underlying error: org.apache.hadoop.hive.ql.parse.SemanticException: Line 1:128 Not yet supported place for UDAF 'count'
    #
    #
    def runFristNonNull(self,column,database,tableName):
        hive_query=" ".join(['select '+column+', count(*) as c from '+database+'.'+tableName+' where '+column+' is not null group by '+column+' order by c desc limit 1;'])
        result=self.hiveUtil.query(hive_query)
        if len(result)>0 :
            return result[0][0]
        else:
            return None
        


    def getStaticsQuery(self,column,value,database,tableName):      
        hive_query=" ".join(["select '"+database+"',"+"'"+tableName+"',"+"'"+column+"',"+'count(*),count('+column+') as count,cast(min('+column+') as string),cast(max('+column+') as string),count(distinct '+column+'),sum(if('+column+' is null,1,0)),cast((sum(if('+column+' is null,1,0))/count('+column+'))  as double),'+"'"+value+"'"+', sum(if('+column+'='+"'"+value+"'"+',1,0)),cast((sum(if('+column+'='+"'"+value+"'"+',1,0))/count('+column+')) as double)  from  '+database+'.'+tableName])
        return hive_query

    def storeToFile(self):
        #self.logging.info("store to file: "+self.resultFile)
        line='Database,Table Name,Column Name, Total Count, Record Count,Min Value,Max Value,Count of distinct value,Count of NULL-valued records,% of NULL-valued Record,#1 Value (count by value in desc order. the 1st non-NULL value),Count of #1 valued record,% of #1 valued record\n'
        for element in self.result:
            line+=element+"\n"
        f = open(self.resultFile,'w')
        f.write(line)
        f.close()



    def storeToFileSpark(self):
        #self.logging.info("store to file: "+self.resultFile)
        line='Database,Table Name,Column Name,count,distinct_count,null_count,null_count_percentage,empty_count,empty_count_percentage,MF_value,MF_value_count,MF_value_count_percentage,MF_value_not_null,MF_value_count_not_null,MF_value_not_null_percentage,min,max,mean,stdev\n'
        for element in self.result:
            line+=element+"\n"
        f = open(self.resultFile,'w')
        f.write(line)
        f.close()

    def storeToFileSparkDetails(self,database,table,columns,customPatternDistribution,topN,topNSynoymns,count,columnDesc):
        for column in columns:
            fileName=self.dataDir+"/"+column+".csv"
            #self.logging.info("store to file: "+fileName)
            f = open(fileName,'w')
                 
            for record in columnDesc:                
                if record['column'] == column:
                    f.write(database+"."+table+":"+column+"\n")
                    f.write(record['column'] +","+record['type'] +","+record['comment'] +"\n")
            for record in customPatternDistribution:
                if record[0] == column:                    
                    f.write("\nPatternDistribution\n")
                    f.write("\nPattern,count,percentage\n")
                    for pair in record[1]:
                        f.write(str(pair[0])+","+str(pair[1])+","+'{0:.4%}'.format(float(pair[1])/count)+"\n")
            for record in topN:
                if record[0] == column: 
                    f.write("\nTopN Value\n")
                    f.write("\nValue,count\n")
                    for pair in record[2]: 
                        f.write(str(pair[0])+","+str(pair[1])+","+'{0:.4%}'.format(float(pair[1])/count)+"\n")
            for record in topNSynoymns:
                if record[0] == column:
                    f.write("\ntopNSynoymns\n")
                    f.write("\nValue,Synoymn,Score\n")
                    for value in record[1]: 
                        for synoymn in value[1]:
                            f.write(str(value[0])+":"+str(synoymn[0])+","+str(synoymn[1])+"\n")
            f.close()
