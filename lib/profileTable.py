"""
    File name: ProfileTable.py
    Author: Guohao Xiao
    Date created: 12/06/2016
    Date last modified: 12/06/2016
    Python Version: 2.7
"""

import time
import sys
import datetime
import subprocess
import os
from HiveUtil import *
from logger import *
from OsUtility import *
class profileTable:
    """
    the ProfileTable class is to profile at database level and table level.
    """
    hiveUtil=None
    def __init__(self,database,tableName,resultDir,logDir):
        log = logger(logDir+"/"+__name__+".log")
        self.logging=log.getLogger(__name__)
        self.hiveUtil=HiveUtil(log)
        self.logging.info("init profileTable")
        self.resultDir=resultDir
        self.tableName=tableName
        self.database=database
        self.result=''
        self.comment=''
        self.partitions=[]
        self.columns=[]
        self.serDe=''
        self.count=''
        self.tableResult='Database,Table Name,Table Comment,Record Count,SerDe Method\n'
        self.columnResult='Database,Table Name,Column Name, Type, Comment, partition Key,Interested,details\n'


    def reset(self):
        self.logging.info("reset profileTable")
        self.result=''
        self.comment=''
        self.partitions=[]
        self.columns=[]
        self.serDe=''
        self.count=''

    def setTableName(self,tableName):
        self.tableName=tableName

    def setDatabase(self,database):
        self.database=database

    def process(self):
        if self.database=='all':
            self.logging.info("Go throuh all the databases")
            databases=self.hiveUtil.showDatabases()
            for database in databases:
                self.logging.info("processing the database:"+database)
                tables=self.hiveUtil.showTables(database)
                for table in tables:
                    self.logging.info("processing the table:"+database+"."+table)
                    result=self.hiveUtil.describeFormattedTable(database,table)
                    print result
                    self.getTableCount(database,table)
                    self.parse(result)
                    self.store(database,table)
                    self.reset()
        if self.database!='all' and self.tableName=='all':
            self.logging.info("Go throuh all the table under "+self.database)
            tables=self.hiveUtil.showTables(self.database)
            print tables
            for table in tables:
                self.logging.info("processing the table:"+self.database+"."+table)
                result=self.hiveUtil.describeFormattedTable(self.database,table)
                self.getTableCount(self.database,table)
                self.parse(result)
                self.store(self.database,table)
                self.reset()
        if self.database!='all' and self.tableName!='all':
           self.logging.info("processing the table:"+self.database+"."+self.tableName)
           result=self.hiveUtil.describeFormattedTable(self.database,self.tableName)
           self.getTableCount(self.database,self.tableName)
           self.parse(result)
           self.store(self.database,self.tableName)
           self.reset()
        self.logging.info("process done")


    def getTableCount(self,database,tableName):
        hive_query="""use {0}; select count(*) as count from {1};""".format(database, tableName)
        if database is None:
            raise Exception("Please provide database name as a parameter.\n")
        if tableName is None:
            raise Exception("Please provide table name as a parameter.\n")
        self.count="".join([x[0] for x in self.hiveUtil.query(hive_query)])


         
            

    def parse(self,result):
        status=''
        for dic in result:
            if dic['column']=='# Partition Information':
               continue
            if dic['column']=='# col_name' and status != 'colomn':
                status='colomn'
                continue
	    if (dic['column']=='# col_name' and  status == 'colomn'):
                status='partition'
                continue
            if dic['column']=='# Detailed Table Information':
                status='Other'
                continue
            if status=='colomn' and dic['column']!='':                
                self.columns.append(dic)
                continue
            if status=='partition' and dic['column']!='':
                self.partitions.append(dic)
                continue
            if dic['type']=='comment':
                self.comment=dic['comment']
            if dic['column']=='SerDe Library:':
                self.serDe=dic['type']
     
    def store(self,database,tableName):
        self.tableResult='Database,Table Name,Table Comment,Record Count,SerDe Method\n'
        self.columnResult='Database,Table Name,Column Name, Type, Comment, partition Key,Interested,details\n'
        self.tableResult+=database+','+tableName+','+self.comment+','+self.count+','+self.serDe
        for element in self.partitions:
            if len(element)<2:
               raise 'error'
            self.columnResult+=database+','+tableName+','+element['column']+","+element['type']+","+element['comment']+",Y,1,1\n"
        for element in self.columns:
            if len(element)<2:
               raise 'error'
            if len(element)>=3:
                comment=''
            self.columnResult+=database+','+tableName+','+element['column']+","+element['type']+","+element['comment']+",N,1,1\n"
        self.writeToFile(self.tableResult,self.resultDir+'/'+database+"_"+tableName+'/','table_result.csv')
        self.writeToFile(self.columnResult,self.resultDir+'/'+database+"_"+tableName+'/','column_result.csv')        
      
    def writeToFile(self,records,dir,fileName):
        make_sure_path_exists(dir)
        f = open(dir+fileName,'w')
        f.write(records)
        f.close()
        self.logging.info("create table level profiling file: "+dir+fileName)
