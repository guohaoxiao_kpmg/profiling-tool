"""
    File name: ProfileColumnWrapper.py
    Author: Guohao Xiao
    Date created: 12/06/2016
    Date last modified: 12/06/2016
    Python Version: 2.7
    improvement: wrapper of profiling columns
"""

import time
import sys
import os
import datetime
#import configparser
from profileColumns import profileColumns
import ConfigParser  
from OsUtility import *

os.environ["PYSPARK_PYTHON"] = "python2"
os.environ['PYTHONPATH'] = ':'.join(sys.path)

if __name__ == "__main__":

    settings = ConfigParser.ConfigParser()  
    settings.read('/home/guohaoxiao/python_hive_tool_cloudera/config.ini')  
    data_dir=settings.get('configure', 'data_dir') 
    log_dir=settings.get('configure', 'log_dir') 
    root_dir=settings.get('configure', 'root_dir')
    lib_dir=settings.get('configure', 'lib_dir')
    engine=settings.get('configure', 'engine')
    thread=settings.get('configure', 'thread')
    TopN_count=settings.get('configure', 'TopN_count')
    TopNValueSynonymns_count=settings.get('configure', 'TopNValueSynonymns_count')
    TopNValueSynonymns_synonymns_count=settings.get('configure', 'TopNValueSynonymns_synonymns_count')
    TopNValueSynonymns_sample_percent=settings.get('configure', 'TopNValueSynonymns_sample_percent')
    spark_log_level=settings.get('configure', 'spark_log_level')
    if sys.argv[1:]:
        file=(str)(sys.argv[1])
        database=(str)(sys.argv[2])
        table=(str)(sys.argv[3])
        engine=(str)(sys.argv[4])
        profileDetails=(str)(sys.argv[5])
    else:
        raise Exception("No parameter: fileName")

    today=getToday()
    data_dir=data_dir+"/"+today
    log_dir=log_dir+"/"+today
    make_sure_path_exists(data_dir)
    make_sure_path_exists(log_dir)
    data_dir=data_dir+"/"+database+"_"+table
    make_sure_path_exists(data_dir)

    handle=profileColumns(file,int(thread),data_dir,log_dir,root_dir,lib_dir,TopN_count,TopNValueSynonymns_count,TopNValueSynonymns_synonymns_count,TopNValueSynonymns_sample_percent,spark_log_level)
    if profileDetails=="false":
        if engine=="hive":
           handle.process()
        elif engine=="spark":
           handle.sparkProcess()
        else:
           raise Exception("cannot recognize engine parameters")
    else:
        if engine=="spark":
            handle.sparkProcess_details()
        else:
            raise Exception("cannot recognize engine parameters")

