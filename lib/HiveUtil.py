"""
    File name: HiveUtil.py
    Author: Guohao Xiao
    Date created: 12/06/2016
    Date last modified: 12/06/2016
    Python Version: 2.7
"""
import os
from StringIO import StringIO
from datetime import datetime
import commands
from collections import *
import subprocess
from logger import *


def run_shell_command(command_line,logging):
  
    logging.debug('Subprocess: "' + command_line + '"')
    try:
        command_line_process = subprocess.Popen(
            command_line,
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE, bufsize=0
        )
        process_output, stdoutput =  command_line_process.communicate()
        logging.debug('Subprocess log\n'+stdoutput)
        logging.debug('Subprocess result\n'+process_output)
    except (OSError) as exception:
        logging.info('Exception occured: ' + str(exception))
        logging.info('Subprocess failed')
        return None
    return process_output

def trimList(list):
    list=[x.strip() for x in list]
    return list

def convertToDic(list,dicList):
    if len(list)!=len(dicList):
       raise Exception("Please provide the same length list and DicList.\n")
    dic={}
    for i in range(0,len(list)):
        dic[dicList[i].strip()]=list[i].strip()
    return dic


class HiveUtil:
    """
    python Hive utility class
    """
    def __init__(self,log):
        self.logging=log.getLogger(__name__)

    def describeTable(self,data_base=None, table_name=None, verbose=True):
        """
         send a describe query for a table and returns dataframe with col name, data type and comments
         :param data_base: database name
         :param table_name: table name
         :param verbose: print info
         :return: data frame
        """
        if data_base is None:
            raise Exception("Please provide database name as a parameter.\n")
        if table_name is None:
            raise Exception("Please provide table name as a parameter.\n")
        hive_query = """USE {0}; describe {1};""".format(data_base, table_name)
        result=[ convertToDic(x,["column","type","comment"]) for x in self.query(hive_query)]        
        return result
    
    def showDatabases(self):
        hive_query = """show databases;"""
        return [x[0] for x in self.query(hive_query)]

            
    def showTables(self,database=None):
        hive_query = "use "+database+ """;show tables;"""
        if database is None:
            raise Exception("Please provide database name as a parameter.\n")
        return [x[0] for x in self.query(hive_query)]

    def describeFormattedTable(self,database=None,tableName=None):
        hive_query = """USE {0}; describe formatted {1};""".format(database, tableName)
        if database is None:
            raise Exception("Please provide database name as a parameter.\n")
        if tableName is None:
            raise Exception("Please provide table name as a parameter.\n")
        result=[convertToDic(x,["column","type","comment"]) for x in self.query(hive_query)]
        return result




    def query(self,hql_query, data_base=None, deliminator="\t", clean_columns=True, add_pyudfs=None,add_pickle=None):
        """
        query a hive table and return data as a dataframe
        :param hql_query: Hive query; string
        :param data_base: HIve database name; string
        :param deliminator: DEFAULTS to "\t", if you have different deliminator then change it
        :param clean_columns: columns names return with table name example mytable.mycol
                              if clean_columns =True if strips table name off and upper cases the name  MYCOL
        :param add_pyudfs: path to the UDF you want; list
        :param add_pickle: path to the Pickle File you want; string list
        :return:
        """
        hive_query=""
        if data_base is not None and len(data_base) > 0:
            hive_query = """USE %s;""" % data_base
        
        if add_pyudfs is not None:
            for udf in add_pyudfs:
                hive_query += """DELETE FILE {0};ADD FILE {0};""".format(udf)
        if add_pickle is not None:
            for pickle in add_pickle:
                hive_query += """DELETE FILE {0};ADD FILE {0};""".format(pickle)
        #hive_query += """set hive.cli.print.header=true;"""
        hive_query += " ".join(hql_query.splitlines())
        cmd = """hive -S -e "%s" """ % hive_query
        result=run_shell_command(cmd,self.logging)
        #status, result = commands.getstatusoutput(cmd)
        #if status > 0:
        #    print("Hive Error. Code %s.\n" % status)
        #    print output
        #    sys.exit(status)
        result=[x.split("\t") for x in result.split("\n") if x is not None and x!=""]
        return result
  
             
    


