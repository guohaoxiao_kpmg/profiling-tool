mailResult() {
  _mailBody=$1
  _mailSubject=$2
  echo $_mailBody | mailx -s "$_mailSubject" -r "$mailSender"  "$mailReceiver"
}

requireParam () {

  local  __resultvar="$1"
  local param=$2
  if [[ "x${param}" == "x" ]]
  then
    echo "ERROR: ${__resultvar} not supplied"
  fi

  eval $__resultvar="'$param'"
}

readConfig () {
  requireParam Dir $1
  rm $Dir"/config.sh"
  tail -n +2  $Dir"/config.ini"| sed -r 's/:/=/g' | while read LINE
  do
    echo $LINE >> $Dir"/config.sh"
  done
  source $Dir"/config.sh"
}


requireProcessSuccess () {
  #checking process exit status: exit with -1 if failed and print out the error message with script name

  if [ $? -ne 0 ]
  then
    #...Argument is optional, but if we call a function before the above test then it changes $? so we do it this way
    local msg='Required process FAILED'
    if [[ "x${1}" != "x" ]]
    then
      msg="${msg} [${1}]"
    fi

    echo "${msg}"
  fi
}

createDirectory() {

  requireParam Dir "${1}"

  if [ -d "${Dir}" ]
  then
    echo "Directory [${Dir}] exists"
  else
    echo "Directory [${Dir}] does not exist"
    mkdir "${Dir}"
    requireProcessSuccess "mkdir ${Dir}"
    echo "Directory [${Dir}] created"   
  fi

}
requireDirectory() {

  requireParam Dir "${1}"

  if [ -d "${Dir}" ]
  then
    echo "Directory [${Dir}] exists"
  else
    echo "Directory [${Dir}] does not exist"
    exit
  fi

}

requireFile () {
  #check the required file exist or not, exist -f if not exist.

  requireParam FILE "${1}"

  if [ -f "${FILE}" ]
  then
    echo "File [${FILE}] exists"
  else
    echo "File [${FILE}] does not exist"
    exit
  fi
}



#==================================================================================================================================
# Time FUNCTIONS
#==================================================================================================================================
declare -A timer
record_start_time (){
  requireParam timer_name "$1"
  timer[start$timer_name]="$(date +"%s")"
  timer[end$timer_name]="$(date +"%s")"
 
}
record_end_time (){
  requireParam timer_name "$1"
  timer[end$timer_name]="$(date +"%s")"
}

time_dif (){
  requireParam timer_name "$2"
  requireParam timer_result "$1"
  diff=$(( ${timer[end$timer_name]}-${timer[start$timer_name]} ))
  eval "$timer_result=\"${diff}\""
  echo "$timer_name Duration: $(($diff / 3600 )) hours $((($diff % 3600) / 60)) minutes $(($diff % 60)) seconds"
}


