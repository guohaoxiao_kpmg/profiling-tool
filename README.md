# README #

To run the profiling process, you need config config.ini at first.

Then you can run the profiling through command line as below:

```
#!python

sh run.sh profile_level(profile_column,profile_table,profile_column_details) engine(spark,hive) database table column_profile_input_file*(e.g column_result.csv)
```


```
#!python

sh run.sh profile_column spark gxiao npi /home/guohaoxiao/data/2017_04_13/gxiao_npi/column_result.csv
```



\* the file is the output of table level profiling, and you need specify which columns you are interested to do with column level profiling.


If you have any questions, please contact: guohaoxiao@kpmg.com